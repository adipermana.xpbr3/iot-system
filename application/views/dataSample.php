<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Begin Page Content -->
<div class="container-fluid mb-3">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Table Data Sample</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Suhu</th>
                            <th>Kelembaban</th>
                            <th>Kadar Nutrisi</th>
                            <th>Tgl Upload</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($sample as $sample) :?>
                        <?php 
                            $date = $sample['created_at'];
                            $set_tgl = date('d-m-Y H:i:s', strtotime($date));    
                        ?> 
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $sample['field1'].' &deg;C' ?></td>
                            <td><?= $sample['field2'].' %' ?></td>
                            <td><?= $sample['field3'].' ppm' ?></td>
                            <td><?= $set_tgl ?></td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


