<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon">
                <i class="fas fa-code"></i>
            </div>
            <div class="sidebar-brand-text mx-3">x-code</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <div class="sidebar-heading">
            Interface
        </div>

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="<?= base_url()?>">
                <i class="fas fa-fw fa-chart-line"></i>
                <span>Dashboard</span></a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="index.html">
                <i class="fas fa-fw fa-calculator"></i>
                <span>Metode Perhitungan</span></a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="<?= base_url('data-sample')?>">
                <i class="fas fa-fw fa-file"></i>
                <span>Data Sample</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
            <h4>Sistem Monitoring IoT Hidroponik</h4>
            </nav>
            <!-- End of Topbar -->

            
        <!-- End of Main Content -->