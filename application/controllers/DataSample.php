<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataSample extends CI_Controller {

    public function index()
    {
        $url="https://api.thingspeak.com/channels/1503718/feeds.json?results=35";
		$get_url = file_get_contents($url);
		$data_json = json_decode($get_url, true);
		$data['sample'] = $data_json['feeds'];
        //var_dump($data); die;
        

        $this->load->view('layouts/header');
        $this->load->view('layouts/sidebar');
        $this->load->view('dataSample', $data);
        $this->load->view('layouts/footer');
    }
}